# renneshiver : parcours hivernaux dans Rennes

Scripts en environnement Windows 10 : MinGW R MikTex Excel

Ces scripts exploitent des données en provenance des bases renneshiver.

## Scripts R
Ces scripts sont dans le dossier "scripts".

## Tex
Le langage Tex est utilisé pour la production des documents.

MikTex est l'environnement utilisé.

Les fichiers .tex sont dans le dossier RENNESHIVER.

